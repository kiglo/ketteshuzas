
let cards_img = new Image();
cards_img.src = 'img/cards.png';
let cardBack = new Image();
cardBack.src = 'img/back.png';
const TRANSITION_TIME = 1;


class Card {
    constructor(id, size, blank = false) {
        this.id = id;
        this.size = size;
        this.position = {x: 0, y: 0, a: 0};
        this.front = true;
        this.blank = blank;
        this.delta = {x: 0, y: 0, a: 0};
        this.completed = true;
        this.visible = true;
        this.clearCallback();
    }
    setPosition(x, y, angle = this.position.a) {
        this.position = {x: x, y: y, a: angle};
    }
    setRotation(angle) {
        this.position.a = angle;
    }
    clearCallback() {
        this.onComplete = ()=>{};
    }
    resize(size) {
        this.position.x /= this.size / size;
        this.position.y /= this.size / size;
        this.size = size;
    }
    moveTo(x, y, angle = this.position.a) {
        this.completed = false;
        this.targetPos = {x:x, y:y, a:angle};
        this.delta.x = (x - this.position.x) / TRANSITION_TIME;
        this.delta.y = (y - this.position.y) / TRANSITION_TIME;
        this.delta.a = (angle - this.position.a) / TRANSITION_TIME;
    }
    update(delta) {
        if (this.completed) return;
        if (delta > 1000) {
            this.position = this.targetPos;
        }
        let dist = objDist(this.targetPos, this.position);
        if (dist > 5) {
            Object.keys(this.position).forEach(k => {
                this.position[k] += this.delta[k] * delta / 1000;
            });
            if (objDist(this.targetPos, this.position) > dist) {
                this.position = this.targetPos;
                this.completed = true;
                this.onComplete();
            }
        } else {
            this.position = this.targetPos;
            this.completed = true;
            this.onComplete();
        }
    }
    contains(x, y) {
        return contains({ x: this.position.x,
                                y:this.position.y,
                                w: this.size, 
                                h:this.size*3/2, 
                                a: this.position.a},
                                {x: x, y: y});
    }
    draw(ctx, pos = this.position) {
        ctx.save();
        if (!this.front && !this.blank) {
            ctx.translate(pos.x + this.size / 2, pos.y + this.size * 3 / 4);
            ctx.rotate(this.position.a);
            ctx.translate(-(pos.x + this.size / 2), -(pos.y + this.size * 3 / 4));
            ctx.drawImage(cardBack, pos.x, pos.y, this.size, this.size * 3 / 2);
            ctx.restore();
            return;
        }
        let p = new Path2D('M 30.444446,0.49999988 H 329.55555 C 346.14478,0.49999988 359.5,13.855223 359.5,30.444446 V 509.55555 C 359.5,526.14478 346.14478,539.5 329.55555,539.5 H 30.444446 C 13.855223,539.5 0.49999988,526.14478 0.49999988,509.55555 V 30.444446 c 0,-16.589223 13.35522312,-29.94444612 29.94444612,-29.94444612 z');
        let t = {x:this.id % 13 * cards_img.width / 13, y: Math.floor(this.id / 13) * cards_img.height / 4};
        let s = this.size/360;
        ctx.translate(pos.x + this.size / 2, pos.y + this.size * 3 / 4);
        ctx.rotate(this.position.a);
        ctx.translate(-(pos.x + this.size / 2), -(pos.y + this.size * 3 / 4));
        ctx.translate(pos.x, pos.y);
        if (this.blank) {
            ctx.scale(s, s);
            ctx.strokeStyle = "black";
            ctx.lineWidth = 2;
            ctx.stroke(p);
            ctx.restore();
            return;
        }
        ctx.drawImage(cards_img, t.x, t.y, cards_img.width/13, cards_img.height/4, 0, 0, this.size, this.size*3/2);
        ctx.restore();
    }
}

class Player {
    constructor(name, size) {
        this.name = name;
        this.size = size;
        this.playing = false;
        this.rotated = false;
        this.deckSpot = new Card(0, size, true);
    }
    setPosition(x, y, rotated = false) {
        this.position = {x: x, y: y};
        this.deckSpot.setPosition(x, y);
        this.rotated = rotated;
    }
    resize(size) {
        this.position.x /= this.size / size;
        this.position.y /= this.size / size;
        this.deckSpot.resize(size);
        this.size = size;
    }
    takeTurn(time) {
        this.totalTime = time;
        this.remaining = time;
    }
    update(delta) {
        if (this.remaining < 0) {
            this.totalTime = -1;
            return;
        }
        this.remaining -= delta;
    }
    draw(ctx){
        let w = this.size * (1 + !this.rotated);
        let h = this.size * 3/2 * (1 + this.rotated);
        ctx.lineWidth = 2;
        ctx.strokeStyle = 'black';
        ctx.fillStyle = 'white';
        ctx.beginPath();
        ctx.arc(this.position.x + (w - this.size) + this.size / 2, this.position.y + (h - this.size * 3 / 2) + this.size / 2, this.size / 3, 0, 360);
        ctx.stroke();
        ctx.strokeRect(this.position.x, this.position.y, w, h);
        ctx.font = this.size / 10 + 'px serif';
        ctx.textBaseline = 'bottom';
        ctx.textAlign = 'center';
        ctx.fillStyle = 'black';
        ctx.fillText(this.name, this.position.x + (w - this.size) + this.size / 2, this.position.y + h - 10, this.size);
        this.deckSpot.draw(ctx);
        if (!this.playing) {
            ctx.fillStyle = '#cccc';
            ctx.fillRect(this.position.x, this.position.y, w, h);
        } else if (this.totalTime > 0){
            ctx.strokeStyle = 'red';
            ctx.lineWidth = '5px';
            ctx.beginPath();
            ctx.arc(this.position.x + (w - this.size) + this.size / 2, this.position.y + (h - this.size * 3 / 2) + this.size / 2, this.size / 3,
                0, 360 * this.totalTime / this.remaining);
            ctx.stroke();
        }
    }
}
class Button {
    constructor(text, x, y, w, h, onClick) {
        this.text = text;
        this.box = {x:x, y:y, w:w, h:h};
        this.onClick = onClick;
    }
    draw(ctx) {
        ctx.fillStyle = 'lightblue';
        ctx.fillRect(this.box.x, this.box.y, this.box.w, this.box.h);
        ctx.fillStyle = 'black';
        ctx.font = '20px serif';
        ctx.textBaseline = 'middle';
        ctx.fillText(this.text, this.box.x + this.box.w/2, this.box.y + this.box.h/2, this.box.w * 0.8);
    }
    handleClick(x, y) {
        if (this.onClick == undefined) return false;
        if (Math.abs(this.box.x + this.box.w/2 - x) > this.box.w/2 || 
            Math.abs(this.box.y + this.box.h/2 - y) > this.box.h/2) return false;
        
        this.onClick();
        return true;
    }
}