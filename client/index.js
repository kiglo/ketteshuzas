let canvas = document.querySelector('canvas');
canvas.addEventListener("click", clicked);
let ctx = canvas.getContext('2d');
const ratio = 3/5;
let BASE_SIZE;
let W, H;
let pw = 0, ph = 0;
const playerPos = [
    [{x:0.5, y:0.8, r:false}],
    [{x:0.5, y:0.8, r:false},{x:0.5, y:0, r:false}],
    [{x:0.5, y:0.8, r:false},{x:0.3, y:0, r:false},{x:0.6, y:0, r:false}],
    [{x:0.5, y:0.8, r:false},{x:0, y:0.5, r:true },{x:0.3, y:0, r:false},{x:0.6, y:0, r:false}],
    [{x:0.5, y:0.8, r:false},{x:0, y:0.5, r:true },{x:0.3, y:0, r:false},{x:0.6, y:0, r:false},{x:0.9, y:0.5, r:true}]
];

function resize() {
    if (canvas.offsetHeight/canvas.offsetWidth > ratio) {
        W = canvas.offsetWidth;
        H = W * ratio;
        ph = (canvas.offsetHeight - H) / 2;
        pw = 0;
    } else {
        H = canvas.offsetHeight;
        W = H / ratio;
        pw = (canvas.offsetWidth - W) / 2;
        ph = 0;
    }
    BASE_SIZE = W / 13;
    cards.forEach((c) => c.resize(BASE_SIZE));
    players.forEach((p) => p.resize(BASE_SIZE));
    canvas.width = canvas.offsetWidth;
    canvas.height = canvas.offsetHeight;
    ctx.resetTransform();
    ctx.fillStyle = 'black';
    ctx.fillRect(0, 0, canvas.width, canvas.height);
    ctx.translate(pw, ph);
}

function clicked(e) {
    if (e.target != canvas && e.target != canvas_anim) {return;}
    ui.forEach((b)=> {if (b.handleClick(e.clientX-pw, e.clientY-ph)) return;});
    let i = cards.length-1;
    while (i >= 0) {
        let c = cards[i];
        if (c.contains(e.clientX-pw, e.clientY-ph)) {
            socket.send(JSON.stringify({type: "pick", data: i}));
            break;
        }
        --i;
    }
}
function init() {
    players = [];
    cards = [];
    ui = [];
    for (let i = 0; i < 52; ++i) {
        cards.push(new Card(i, BASE_SIZE));
        cards[i].setPosition(0, 0, 0);
    }
    ui.push(new Button("Leave game", 10, 10, 100, 40, () => {if (window.confirm("Are you sure?")) leaveGame();}));
    window.requestAnimationFrame(loop);
}

function placeCards(seed) {
    let random = new Math.seedrandom(seed);
    cardsOrd = [];
    for (let i = 0; i < 52; ++i) {
        cardsOrd.push(i);
        cards[i].front = false;
        cards[i].clearCallback();
        cards[i].moveTo(random() * W / 2 + W / 4, random() * H / 2 + H / 4, random() * 2 * Math.PI);
    }
    ui = ui.filter((b) => b.text != "Start new");
}

function placePlayers(names) {
    players = [];
    let pos = playerPos[names.length-1];
    for (let i = 0; i < names.length; ++i) {
        players.push(new Player(names[i], BASE_SIZE));
        players[i].setPosition(W * pos[i].x, H * pos[i].y, pos[i].r);
    }
}

function update(delta) {
    cards.forEach((c)=>c.update(delta));
    players.forEach((p) => p.update(delta));
}

function render() {
    ctx.fillStyle = 'white';
    ctx.fillRect(0, 0, W, H);
    players.forEach((p)=>p.draw(ctx));
    cardsOrd.forEach((c)=>cards[c].draw(ctx)); 
    ui.forEach((e)=>e.draw(ctx));
    if (timer <= 0) return;
    ctx.fillStyle = 'black';
    ctx.font = '15px serif';
    ctx.textAlign = 'left';
    // ctx.fillText("Time remaining: " + timer / 1000, 10, H - 25, 150);
}

function loop(time) {
    let delta = time - lastTime;
    timer += delta;
    lastTime = time;
    // if (delta > 30)
    // console.log(delta);
    update(delta);
    render();
    window.requestAnimationFrame(loop);
}

let lastTime = 0;
let timer = 0;
let pickTime = 0;
let currentPlayer = 0;
let cards = [];
let cardsOrd = [];
let players = [];
let ui = [];
window.addEventListener('resize', resize);
resize();
init();