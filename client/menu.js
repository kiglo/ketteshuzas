// const serverAddress = 'https://lit-falls-44136.herokuapp.com';
const serverAddress = 'http://localhost:3000';
const gameRoot = serverAddress + '/ketteshuzas';

$(document).ready(()=>{
    nameInput.val(name);
    $("#token").click(() => {
        socket.send(JSON.stringify({type: "token", data: nameInput.val()}));
    });
    socket.send(JSON.stringify({type: 'validate', data: token}));
});

let overlay = $('#overlay');
let nameInput = $('#name');
let gameList = $('#games');
let gameName = $('#game-name');
let gamePass = $('#game-pass');
let gameMax = $('#max');
let msg = $("#message");
let nameMsg = $("#name-msg");
let name = localStorage.getItem('name') || '';
let token = localStorage.getItem('token') || '';
let socket = new WebSocket("ws://localhost:3000");

function joinGame(name, needpassword) {
    let pass = '';
    if (needpassword) {
        pass = window.prompt("Enter password");
    }
    socket.send(JSON.stringify({type: 'join', data: {name: name, pass: pass}}));
}

function leaveGame() {
    socket.send(JSON.stringify({type: "leave"}));
    overlay.css('display', 'block');
    socket.send(JSON.stringify({type: "list"}));
}

function sendMessage() {
    socket.send(JSON.stringify({type: "chat", data: $('#new-message').val()}));
    $('#new-message').val('');
    return false;
}

function newMessage(sender, msg) {
    let cls = (sender == "Console") ? 'console' : ( sender == name ? 'self' : 'player');
    $('#messages').append(`<div class="chat-msg ${cls}"><span>${sender}:</span><span>${msg}</span></div>`);
    $('#messages').animate({ scrollTop: $("#messages").height()/2 }, 1000);
}

function listGames(list) {
    let html = '<div>';
    list.forEach(game => {
        html += `<div class="game ${game.pass ? "pass" : ""}"><span>${game.name}</span><br>
                <span>${game.state} </span><span>${game.players}</span><br>
                <button onClick="joinGame('${game.name}', ${game.pass})">Belépés</button></div>`;
    });
    gameList.html(html + '</div>');
}

function newGame() {
    socket.send(JSON.stringify({type: "create", data: {name: gameName.val(), pass: gamePass.val(), maxp: gameMax.val()}}));
}