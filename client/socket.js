socket.onmessage = (msg) => {
    msg = JSON.parse(msg.data);
    console.log(msg);
    if (!!msg.error) {
        console.log(msg.error);
        return;
    }
    switch (msg.type) {
        case 'token':
            token = msg.data;
            localStorage.setItem('token', token);
            localStorage.setItem('name', nameInput.val());
            break;
        case 'validate':
            socket.send(JSON.stringify({type: "list"}));
            break;
        case 'list':
            listGames(msg.data);
            break;
        case 'join':
            overlay.css('display', 'none');
            break;
        case 'create':
            socket.send(JSON.stringify({type: "list"}));
            gameName.val('');
            gamePass.val('');
            msg.html('');
            break;
        case 'player joined':
            names = moveToEnd(msg.data, msg.data.indexOf(name));
            names.pop();
            names.unshift(name);
            placePlayers(names);
            break;
        case 'start':
            pickTime = msg.data.timeout;
            placeCards(msg.data.seed);
            break;
        case 'chat':
            newMessage(msg.data.sender, msg.data.msg);
            break;
        case 'next':
            let i = players.findIndex((p) => {return p.name == name;});
            if (i >= 0) {
                players[i].takeTurn(pickTime);
            }
            break;
        case 'picked':
            let p = players[msg.data.p].deckSpot.position;
            if (cards[msg.data.c].completed) {
                cards[msg.data.c].id = msg.data.id;
                cards[msg.data.c].onComplete = () => {cards[msg.data.c].front = true;}
                cards[msg.data.c].moveTo(p.x, p.y, p.a);
            }
            cardsOrd = moveToEnd(cardsOrd, cardsOrd.indexOf(msg.data.c));
        case 'gameover':
            ui.push(new Button("Start new", 10, 60, 100, 40, () => {socket.send(JSON.stringify({type: "start"})); console.log('start')}));
            break;
    }
}
/*
socket.on('welcome', () => {
    socket.emit('validate', nameInput.val());
});
socket.on('create', (res) => {
    if (res == "OK") {
        gameName.val('');
        gamePass.val('');
        msg.html('');
        listGames();
    } else {
        msg.html(res);
    }
});
socket.on('validate', (res) => {
    if (res != "OK") {
        nameMsg.html(res);
        return;
    }
    nameMsg.html('');
    name = nameInput.val();
    window.localStorage.setItem('name', name);
});
socket.on('join', (res) => {
    if (res == "OK") {
        msg.html('');
        overlay.css('display', 'none');
    } else {
        msg.html(res);
    }
});
socket.on('start', (data) => {
    pickTime = data.timeout;
    placeCards(data.seed);
});

socket.on('player joined', (names) => {
    names = moveToEnd(names, names.indexOf(name));
    names.pop();
    names.unshift(name);
    placePlayers(names);
});

socket.on('player left', (name) => {
    console.log(`${name} has left`);
});

socket.on('chat', (msg) => {
    newMessage(msg.p, msg.m);
});

socket.on('picked', (data) => {
    let p = players[data.p].deckSpot.position;
    if (cards[data.c].completed) {
        cards[data.c].id = data.id;
        cards[data.c].onComplete = () => {cards[data.c].front = true;}
        cards[data.c].moveTo(p.x, p.y, p.a);
    }
    cardsOrd = moveToEnd(cardsOrd, cardsOrd.indexOf(data.c));
});

socket.on('next', (name) => {
    let i = players.findIndex((p) => {return p.name == name;});
    if (i >= 0) {
        players[i].takeTurn(pickTime);
    }
});

socket.on('gameover', (winner) => {
    ui.push(new Button("Start new", 10, 60, 100, 40, () => {socket.emit('start'); console.log('start')}));
});
*/