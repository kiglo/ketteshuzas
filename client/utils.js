function contains(rect, point) {
    let p = {x: point.x - rect.x - rect.w/2, y: point.y - rect.y - rect.h/2};
    let x = Math.cos(rect.a) * p.x + Math.sin(rect.a) * p.y;
    let y = -Math.sin(rect.a) * p.x + Math.cos(rect.a) * p.y;

    return Math.abs(x) <= rect.w / 2 && Math.abs(y) <= rect.h / 2;
}
function objDist(o1, o2) {
    let d = 0;
    Object.keys(o1).forEach(k => {
        d += Math.abs(o1[k] - o2[k])
    });
    return d;
}

function moveToEnd(arr, i) {
    let first = arr.slice(0, i);
    let second = arr.slice(i+1);
    second.push(arr[i]);
    return first.concat(second);
}