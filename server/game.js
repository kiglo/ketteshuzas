const player = require('./player');
const GameState = {IDLE: 'idle', PLAYING: 'playing'};
const PICK_TIMEOUT = 5000;


function shuffle(arr) {
    //Fisher-Yates Algorithm
    let l = arr.length, temp, index;
    while (l > 0) {
        index = Math.floor(Math.random() * l);
        l--;
        temp = arr[l];
        arr[l] = arr[index];
        arr[index] = temp;
    }
    return arr;
}

exports.GameState = GameState;
exports.Game = class {
    constructor(name, pass = '', maxp) {
        this.name = name;
        this.pass = pass;
        this.state = GameState.IDLE;
        this.players = {};
        this.maxPlayers = parseInt(maxp);
        this.error = "";
        if (name.length < 3) this.error = "Name is too short";
        if (isNaN(this.maxPlayers) || this.maxPlayers > 5 || this.maxPlayers < 2) this.error = "Invalid player count";
    }
    info() {
        return {name:this.name, state:this.state, pass:this.pass.length>0,
                 players:Object.keys(this.players).length + '/' + this.maxPlayers};
    }
    get_error() {   
        return this.error;
    }
    join(id, p, pass = '') {
        if (p.state != player.PlayerState.IDLE) return "Player in game";
        if (Object.keys(this.players).length >= this.maxPlayers) return "Room is full";
        if (pass != this.pass) return "Invalid password";
        if (this.state != GameState.IDLE) return "Game is running";

        p.state = player.PlayerState.WAITING;
        p.game = this;
        this.players[id] = p;
        let names = [];
        Object.keys(this.players).forEach((p) => {
            names.push(this.players[p].name);
        });
        this.sendAll('player joined', names);

        if (Object.keys(this.players).length == this.maxPlayers) {
            this.start();
        }
        return null;
    }
    leave(id) {
        this.sendAll("player left", this.players[id].name);
        delete this.players[id];
        return Object.keys(this.players).length == 0;
    }
    chat(sender, msg, id) {
        if (!!id) {
            this.send(this.players[id].socket, {type: 'chat', data: {sender: sender, msg: msg}});
            return;
        }
        this.sendAll("chat", {sender: sender, msg: msg});
    }
    sendAll(s, data) {
        for (const p in this.players) {
            this.send(this.players[p].socket, {type: s, data: data});
        }
    }
    start() {
        this.state = GameState.PLAYING;
        this.currentPlayer = Math.floor(Math.random() * Object.keys(this.players).length);
        Object.keys(this.players).forEach((p) => {
            this.players[p].state = player.PlayerState.PLAYING;
        });
        this.cards = shuffle(Array.from(Array(52).keys()));
        this.activePlayers = new Set(Object.keys(this.players));
        this.chat("Console", "Game started");
        this.sendAll('start', {seed: Math.random(), timeout: PICK_TIMEOUT});
        this.nextPlayer();
    }
    gameover() {
        clearTimeout(this.timeout);
        this.state = GameState.IDLE;
        if (this.activePlayers.size == 1) {
            const it = this.activePlayers.values();
            this.sendAll('gameover',it.next().value );
        }
        this.activePlayers.clear();
    }
    ready(id) {
        this.activePlayers.add(id);
        if (this.activePlayers.size >= 2) {
            this.start();
        }
    }
    nextPlayer() {
        console.log(this.currentPlayer);
        this.currentPlayer = (this.currentPlayer + 1) % Object.keys(this.players).length;
        let id = Object.keys(this.players)[this.currentPlayer];
        if (this.players[id] == undefined || 
            this.players[id].state != player.PlayerState.PLAYING &&
            this.players[id].state != player.PlayerState.WAITING) {this.nextPlayer(); return;};
        this.sendAll('next', this.players[id].name);
        this.timeout = setTimeout(this.pick.bind(this), PICK_TIMEOUT, id, -1);
    }
    pick(id, card) {
        if (Object.keys(this.players)[this.currentPlayer] != id) {
            if (card >= 0)
                this.chat("Console", "Not your turn", id);
            return;
        }
        clearTimeout(this.timeout);
        if (card == -1) {card = this.cards.findIndex((c) => {return c != undefined;})}
        let c = this.cards[card];
        if (c == undefined) {
            this.chat("Console", "Invalid pick", id);
            return;
        }
        delete this.cards[card];
        this.sendAll('picked', {p: this.currentPlayer, c: card, id: c});
        if ((c % 13) == 1) {
            this.players[id].state = player.PlayerState.WAITING;
            this.activePlayers.delete(id);
            if (this.activePlayers.size <= 1) {
                this.gameover();
                return;
            }
        }
        this.nextPlayer();
    }
    receive(id, msg) {
        switch (msg.type) {
            case 'pick':
                this.pick(id, msg.data);
                break;
            case 'start':
                this.ready(id);
                break;
            case 'chat':
                if (this.players[id] !== undefined && msg.data)
                    this.chat(this.players[id].name, msg.data);
                break;
        }
        console.log(msg);
    }
}