const Player    = require('./player');
const Game      = require('./game');

exports.GameManager = class {
    constructor(wss) {
        this.wss = wss;
        this.games = [];
        this.players = {};
    }

    connect(socket) {
        this.players[socket.id] = new Player.Player("", socket);
    }
    disconnect(socket) {
        delete this.players[socket.id];
    }
}