var ws = require('ws');
const gdCom = require('@gd-com/utils')
const port = process.env.PORT || 3000;
const game = require('./game');
const player = require('./player');

function send(socket, data) {
    // let packet = gdCom.putVar(data);
    // socket.send(packet)
    socket.send(JSON.stringify(data));
}

function receive(msg) {
    // gdCom.getVar(msg).value
    return JSON.parse(msg);
}

function token() {
    rand = () => Math.random().toString(36).substr(2);
    return rand()+rand()+rand();
}

function register(socket, name) {
    if (typeof(name) != "string" || name.length < 3) {
        send(socket, {type: "token", data: null, error: "Invalid name"});
        return;
    }
    socket.token = token();
    players[socket.token] = new player.Player(name, socket);
    send(socket, {type: "token", data: socket.token, error: null});
}

function validate(socket, token) {
    if (token in players) {
        socket.token = token;
        send(socket, {type: "validate", data: players[token].state, error: null});
        return true;
    }
    send(socket, {type: "validate", data: null, error: "Invalid token"});
    return false;
}

function send_list(socket) {
    let list = [];
    for (const g in games) {
        list.push(games[g].info());
    }
    send(socket, {type: "list", data: list, error: null});
}

function join(socket, name, pass) {
    if (!validate(socket, socket.token)) return;
    for (const g in games) {
        if (games[g].name == name) {
            send(socket, {type: "join", data: null, error: games[g].join(socket.token, players[socket.token], pass)});
            return;
        }
    }
    send(socket, {type: "join", data: null, error: "Invalid game"});
}

function create(socket, data) {
    if (!validate(socket, socket.token)) return;
    for (const g in games) {
        if (games[g].name == data.name) {
            send(socket, {type: "create", data: null, error: "Game exists"});
            return;
        }
    }
    let g = new game.Game(data.name, data.pass, data.maxp);
    if (g.get_error().length > 0) {
        send(socket, {tyep: "create", data: null, error: g.get_error()});
        return;
    }
    g.send = send;
    games.push(g);
    join(socket, data.name, data.pass);
}

const wss = new ws.Server({port: port});
wss.on('connection', socket => {
    socket.on("message", msg => {
        msg = receive(msg);
        switch (msg.type) {
            case "token":
                register(socket, msg.data);
                break;
            case "validate":
                validate(socket, msg.data);
                break;
            case "list":
                send_list(socket);
                break;
            case "create":
                create(socket, msg.data);
                break;
            case "join":
                join(socket, msg.data.name, msg.data.pass);
                break;
            default:
                players[socket.token].game.receive(socket.token, msg);
                break;

        }
    });
    socket.on("close", ()=>{
        console.log("disconnected");
    });
});

let games = [];
let players = {};
