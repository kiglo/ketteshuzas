const PlayerState = {IDLE: 'idle', INGAME: 'ingame', WAITING: 'waiting', PLAYING: 'playing'};

exports.PlayerState = PlayerState;
exports.Player = class {
    constructor(name, socket) {
        this.name = name;
        this.state = PlayerState.IDLE;
        this.socket = socket;
    }
    reset() {
        this.game = undefined;
        this.state = PlayerState.IDLE;
    }
}